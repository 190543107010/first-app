package com.example.malefemalegenerater;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.malefemalegenerater.adapter.userlistadapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class second extends AppCompatActivity {

    ListView lvuser;
    ArrayList<HashMap<String,Object>> usrlist = new ArrayList<>();
    userlistadapter  useradapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondactivity);
        datareferance();
        dataevent();
    }
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
    void datareferance()
    {
        lvuser = findViewById(R.id.lstdata);
    }
    void dataevent()
    {
        usrlist.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra( "userlst"));
        useradapter = new userlistadapter(this,usrlist);
        lvuser.setAdapter(useradapter);


    }

}
