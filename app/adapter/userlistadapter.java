package com.example.radioandcheckbox.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.radioandcheckbox.R;
import com.example.radioandcheckbox.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class userlistadapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String,Object>> usrlist;

    public userlistadapter(Context context,ArrayList<HashMap<String,Object>> usrlist) {
        this.context = context;
        this.usrlist = usrlist;
    }

    @Override
    public int getCount() {
        return usrlist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_users,null);
        TextView tvfnm = view1.findViewById(R.id.tvfname);
        TextView tvlnm = view1.findViewById(R.id.tvlname);
        TextView tveml = view1.findViewById(R.id.tvemail);
        TextView tvgen = view1.findViewById(R.id.tvgender);
        TextView tvhoby = view1.findViewById(R.id.tvhobby);

        tvfnm.setText(String.valueOf(usrlist.get(position).get(Const.first_name)));
        tvlnm.setText(String.valueOf(usrlist.get(position).get(Const.last_name)));
        tvgen.setText(String.valueOf(usrlist.get(position).get(Const.gender)));
        tveml.setText(String.valueOf(usrlist.get(position).get(Const.email)));
        tvhoby.setText(String.valueOf(usrlist.get(position).get(Const.hobby)));
        return view1;
    }
}
