package com.example.radioandcheckbox;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.radioandcheckbox.adapter.userlistadapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class second extends AppCompatActivity {

    ListView lvuser;
    ArrayList<HashMap<String,Object>> usrlist = new ArrayList<>();
    userlistadapter  useradapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second);
        datareferance();
        dataevent();
    }
    void datareferance()
    {
        lvuser = findViewById(R.id.lstdata);
    }
    void dataevent()
    {
        usrlist.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra( "userlst"));
        useradapter = new userlistadapter(this,usrlist);
        lvuser.setAdapter(useradapter);
    }

}
